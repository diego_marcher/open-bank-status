package com.openbank.environmenttest.login;

public class OAuthToken {

    public String cookieCredential;
    public String tokenCredential;
    public String bankOriginalToken;

    public String getCookieCredential() {
        return cookieCredential;
    }

    public void setCookieCredential(String cookieCredential) {
        this.cookieCredential = cookieCredential;
    }

    public String getTokenCredential() {
        return tokenCredential;
    }

    public void setTokenCredential(String tokenCredential) {
        this.tokenCredential = tokenCredential;
    }

    public int getExpiresIn() {
        return 0;
    }

    public String getRefreshToken() {
        return null;
    }

    public String getBankOriginalToken() {
        return bankOriginalToken;
    }

    public void setBankOriginalToken(String bankOriginalToken) {
        this.bankOriginalToken = bankOriginalToken;
    }
}

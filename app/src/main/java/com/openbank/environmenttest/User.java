package com.openbank.environmenttest;

public class User {
    private String document;
    private String documentType;
    private String password;

    public User(String document, String documentType, String password) {
        this.document = document;
        this.documentType = documentType;
        this.password = password;
    }

    public String getDocument() {
        return document;
    }

    public String getDocumentType() {
        return documentType;
    }

    public String getPassword() {
        return password;
    }
}

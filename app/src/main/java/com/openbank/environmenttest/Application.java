package com.openbank.environmenttest;

import com.openbank.environmenttest.ServerParameters.DEV;
import com.openbank.environmenttest.ServerParameters.PROD;
import com.openbank.environmenttest.ServerParameters.QA;
import com.openbank.environmenttest.ServerParameters.STAGING;
import com.openbank.environmenttest.interfaces.OauthRetrofitService;

import java.util.concurrent.TimeUnit;

import okhttp3.CertificatePinner;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Application extends android.app.Application {

    public static OauthRetrofitService DEV_OAUTH_SERVICE;
    public static OauthRetrofitService QA_OAUTH_SERVICE;
    public static OauthRetrofitService STAGING_OAUTH_SERVICE;
    public static OauthRetrofitService PROD_OAUTH_SERVICE;

    @Override
    public void onCreate() {
        super.onCreate();
        setDEVEndPoint();
        setQAEndPoint();
        setSTAGINGEndPoint();
        setPRODEndPoint();
    }

    public void setDEVEndPoint() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClientPostLoginBuilder = new OkHttpClient.Builder();
        httpClientPostLoginBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientPostLoginBuilder.interceptors().add(logging);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        httpClientBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientBuilder.interceptors().add(logging);

        CertificatePinner certificatePinner = new CertificatePinner.Builder()
                .add(DEV.domain, DEV.certificates_pin_1)
                .add(DEV.domain, DEV.certificates_pin_2)
                .add(DEV.domain, DEV.certificates_pin_3)
                .add(DEV.domain, DEV.certificates_pin_4)
                .build();

        httpClientPostLoginBuilder.certificatePinner(certificatePinner);
        httpClientBuilder.certificatePinner(certificatePinner);

        OkHttpClient httpClient = httpClientBuilder.build();

        Retrofit oauthRetrofit = new Retrofit.Builder()
                .baseUrl(DEV.base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        DEV_OAUTH_SERVICE = oauthRetrofit.create(OauthRetrofitService.class);
    }

    public void setQAEndPoint() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClientPostLoginBuilder = new OkHttpClient.Builder();
        httpClientPostLoginBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientPostLoginBuilder.interceptors().add(logging);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        httpClientBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientBuilder.interceptors().add(logging);

        CertificatePinner certificatePinner = new CertificatePinner.Builder()
                .add(QA.domain, QA.certificates_pin_1)
                .add(QA.domain, QA.certificates_pin_2)
                .add(QA.domain, QA.certificates_pin_3)
                .add(QA.domain, QA.certificates_pin_4)
                .build();

        httpClientPostLoginBuilder.certificatePinner(certificatePinner);
        httpClientBuilder.certificatePinner(certificatePinner);

        OkHttpClient httpClient = httpClientBuilder.build();

        Retrofit oauthRetrofit = new Retrofit.Builder()
                .baseUrl(QA.base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        QA_OAUTH_SERVICE = oauthRetrofit.create(OauthRetrofitService.class);
    }

    public void setSTAGINGEndPoint() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClientPostLoginBuilder = new OkHttpClient.Builder();
        httpClientPostLoginBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientPostLoginBuilder.interceptors().add(logging);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        httpClientBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientBuilder.interceptors().add(logging);

        CertificatePinner certificatePinner = new CertificatePinner.Builder()
                .add(STAGING.domain, STAGING.certificates_pin_1)
                .add(STAGING.domain, STAGING.certificates_pin_2)
                .add(STAGING.domain, STAGING.certificates_pin_3)
                .add(STAGING.domain, STAGING.certificates_pin_4)
                .build();

        httpClientPostLoginBuilder.certificatePinner(certificatePinner);
        httpClientBuilder.certificatePinner(certificatePinner);

        OkHttpClient httpClient = httpClientBuilder.build();

        Retrofit oauthRetrofit = new Retrofit.Builder()
                .baseUrl(STAGING.base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        STAGING_OAUTH_SERVICE = oauthRetrofit.create(OauthRetrofitService.class);
    }

    public void setPRODEndPoint() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();

        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClientPostLoginBuilder = new OkHttpClient.Builder();
        httpClientPostLoginBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientPostLoginBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientPostLoginBuilder.interceptors().add(logging);

        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder();

        httpClientBuilder.connectTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.readTimeout(120, TimeUnit.SECONDS);
        httpClientBuilder.writeTimeout(120, TimeUnit.SECONDS);

        httpClientBuilder.interceptors().add(logging);

        CertificatePinner certificatePinner = new CertificatePinner.Builder()
                .add(PROD.domain, PROD.certificates_pin_1)
                .add(PROD.domain, PROD.certificates_pin_2)
                .add(PROD.domain, PROD.certificates_pin_3)
                .add(PROD.domain, PROD.certificates_pin_4)
                .build();

        httpClientPostLoginBuilder.certificatePinner(certificatePinner);
        httpClientBuilder.certificatePinner(certificatePinner);

        OkHttpClient httpClient = httpClientBuilder.build();

        Retrofit oauthRetrofit = new Retrofit.Builder()
                .baseUrl(PROD.base_url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        PROD_OAUTH_SERVICE = oauthRetrofit.create(OauthRetrofitService.class);
    }
}

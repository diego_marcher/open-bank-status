package com.openbank.environmenttest;

public class ServerParameters {

    public static final class DEV {
        public static final String base_url = "https://apidevalb001.aws.ok-cloud.net";
        public static final String domain = "*.aws.ok-cloud.net";
        public static final String certificates_pin_1 = "sha256/MkkEuqmkHoLmmrcPWO79Q+h83rRbmdeV0t1pfBP6mVE=";
        public static final String certificates_pin_2 = "sha256/JSMzqOOrtyOT1kmau6zKhgT676hGgczD5VMdRMyJZFA=";
        public static final String certificates_pin_3 = "sha256/++MBgDH5WGvL9Bcn5Be30cRcL0f5O+NyoXuWtQdX1aI=";
        public static final String certificates_pin_4 = "sha256/KwccWaCgrnaw6tsrrSO61FgLacNgG2MMLq8GE6+oP5I=";
    }

    public static final class QA {
        public static final String base_url = "https://apitstalb001.aws.ok-cloud.net";
        public static final String domain = "*.aws.ok-cloud.net";
        public static final String certificates_pin_1 = "sha256/MkkEuqmkHoLmmrcPWO79Q+h83rRbmdeV0t1pfBP6mVE=";
        public static final String certificates_pin_2 = "sha256/JSMzqOOrtyOT1kmau6zKhgT676hGgczD5VMdRMyJZFA=";
        public static final String certificates_pin_3 = "sha256/++MBgDH5WGvL9Bcn5Be30cRcL0f5O+NyoXuWtQdX1aI=";
        public static final String certificates_pin_4 = "sha256/KwccWaCgrnaw6tsrrSO61FgLacNgG2MMLq8GE6+oP5I=";
    }

    public static final class STAGING {
        public static final String base_url = "https://apistgalb001.aws.ok-cloud.net";
        public static final String domain = "*.aws.ok-cloud.net";
        public static final String certificates_pin_1 = "sha256/MkkEuqmkHoLmmrcPWO79Q+h83rRbmdeV0t1pfBP6mVE=";
        public static final String certificates_pin_2 = "sha256/JSMzqOOrtyOT1kmau6zKhgT676hGgczD5VMdRMyJZFA=";
        public static final String certificates_pin_3 = "sha256/++MBgDH5WGvL9Bcn5Be30cRcL0f5O+NyoXuWtQdX1aI=";
        public static final String certificates_pin_4 = "sha256/KwccWaCgrnaw6tsrrSO61FgLacNgG2MMLq8GE6+oP5I=";
    }

    public static final class PROD {
        public static final String base_url = "https://api.openbank.es";
        public static final String domain = "api.openbank.es";
        public static final String certificates_pin_1 = "sha256/RCc8QsQjf8EaDcqKIjzTxKDSG6AnlZlc9puMuvhWdr0=";
        public static final String certificates_pin_2 = "sha256/gMxWOrX4PMQesK9qFNbYBxjBfjUvlkn/vN1n+L9lE5E=";
        public static final String certificates_pin_3 = "sha256/JbQbUG5JMJUoI6brnx0x3vZF6jilxsapbXGVfjhN8Fg=";
        public static final String certificates_pin_4 = "sha256/JbQbUG5JMJUoI6brnx0x3vZF6jilxsapbXGVfjhN8Fg=";
    }
}

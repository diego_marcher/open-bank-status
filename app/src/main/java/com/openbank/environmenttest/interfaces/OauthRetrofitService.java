package com.openbank.environmenttest.interfaces;

import com.google.gson.JsonObject;
import com.openbank.environmenttest.login.OAuthToken;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface OauthRetrofitService {

    String ACCEPT_JSON_HEADER = "Accept: application/json";

    @Headers({ACCEPT_JSON_HEADER})
    @POST("/v2/authentication")
    Call<OAuthToken> getToken(@Body JsonObject json);

}

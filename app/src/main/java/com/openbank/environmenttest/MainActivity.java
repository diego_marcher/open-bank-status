package com.openbank.environmenttest;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.openbank.environmenttest.login.OAuthToken;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    private static final User USER = new User("05415535R", "N", "1234");
    private static final String EXECUTING = "Agantiaaaa";

    @BindView(R.id.dev_login_status) ImageView devLoginStatus;
    @BindView(R.id.dev_login_status_code) TextView devLoginStatusCode;

    @BindView(R.id.qa_login_status) ImageView qaLoginStatus;
    @BindView(R.id.qa_login_status_code) TextView qaLoginStatusCode;

    @BindView(R.id.staging_login_status) ImageView stagingLoginStatus;
    @BindView(R.id.staging_login_status_code) TextView stagingLoginStatusCode;

    @BindView(R.id.prod_login_status) ImageView prodLoginStatus;
    @BindView(R.id.prod_login_status_code) TextView prodLoginStatusCode;

    private Call<OAuthToken> devLoginCall;
    private Call<OAuthToken> qaLoginCall;
    private Call<OAuthToken> stagingLoginCall;
    private Call<OAuthToken> prodLoginCall;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        attemptMultipleLogin();
    }

    private void attemptMultipleLogin() {
        devLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
        qaLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
        stagingLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));
        prodLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGray));

        devLoginStatusCode.setText(EXECUTING);
        qaLoginStatusCode.setText(EXECUTING);
        stagingLoginStatusCode.setText(EXECUTING);
        prodLoginStatusCode.setText(EXECUTING);

        attemptDevLogin();
        attemptQaLogin();
        attemptStagingLogin();
        attemptProdLogin();
    }

    @OnClick(R.id.refresh_btn)
    public void onRefreshBtnClick() {
        attemptMultipleLogin();
    }

    private void attemptDevLogin() {
        if (devLoginCall != null) {
            devLoginCall.cancel();
        }
        devLoginCall = Application.DEV_OAUTH_SERVICE.getToken(buildJsonObject(USER));
        devLoginCall.enqueue(new Callback<OAuthToken>() {
            @Override
            public void onResponse(Call<OAuthToken> call, retrofit2.Response<OAuthToken> response) {
                if (response.isSuccessful()) {
                    devLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen));
                    devLoginStatusCode.setVisibility(View.INVISIBLE);
                } else {
                    devLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                    devLoginStatusCode.setText(String.valueOf(response.code()));
                    devLoginStatusCode.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<OAuthToken> call, Throwable t) {
                devLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                devLoginStatusCode.setText(String.valueOf("Failed"));
                devLoginStatusCode.setVisibility(View.VISIBLE);
            }
        });
    }

    private void attemptQaLogin() {
        if (qaLoginCall != null) {
            qaLoginCall.cancel();
        }
        qaLoginCall = Application.QA_OAUTH_SERVICE.getToken(buildJsonObject(USER));
        qaLoginCall.enqueue(new Callback<OAuthToken>() {
            @Override
            public void onResponse(Call<OAuthToken> call, retrofit2.Response<OAuthToken> response) {
                if (response.isSuccessful()) {
                    qaLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen));
                    qaLoginStatusCode.setVisibility(View.INVISIBLE);
                } else {
                    qaLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                    qaLoginStatusCode.setText(String.valueOf(response.code()));
                    qaLoginStatusCode.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<OAuthToken> call, Throwable t) {
                qaLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                qaLoginStatusCode.setText(String.valueOf("Failed"));
                qaLoginStatusCode.setVisibility(View.VISIBLE);
            }
        });
    }

    private void attemptStagingLogin() {
        if (stagingLoginCall != null) {
            stagingLoginCall.cancel();
        }
        stagingLoginCall = Application.STAGING_OAUTH_SERVICE.getToken(buildJsonObject(USER));
        stagingLoginCall.enqueue(new Callback<OAuthToken>() {
            @Override
            public void onResponse(Call<OAuthToken> call, retrofit2.Response<OAuthToken> response) {
                if (response.isSuccessful()) {
                    stagingLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen));
                    stagingLoginStatusCode.setVisibility(View.INVISIBLE);
                } else {
                    stagingLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                    stagingLoginStatusCode.setText(String.valueOf(response.code()));
                    stagingLoginStatusCode.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<OAuthToken> call, Throwable t) {
                stagingLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                stagingLoginStatusCode.setText(String.valueOf("Failed"));
                stagingLoginStatusCode.setVisibility(View.VISIBLE);
            }
        });
    }

    private void attemptProdLogin() {
        if (prodLoginCall != null) {
            prodLoginCall.cancel();
        }
        prodLoginCall = Application.PROD_OAUTH_SERVICE.getToken(buildJsonObject(USER));
        prodLoginCall.enqueue(new Callback<OAuthToken>() {
            @Override
            public void onResponse(Call<OAuthToken> call, retrofit2.Response<OAuthToken> response) {
                if (response.isSuccessful()) {
                    prodLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen));
                    prodLoginStatusCode.setVisibility(View.INVISIBLE);
                } else {
                    prodLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                    prodLoginStatusCode.setText(String.valueOf(response.code()));
                    prodLoginStatusCode.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<OAuthToken> call, Throwable t) {
                prodLoginStatus.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed));
                prodLoginStatusCode.setText(String.valueOf("Failed"));
                prodLoginStatusCode.setVisibility(View.VISIBLE);
            }
        });
    }

    private JsonObject buildJsonObject(User user) {
        JsonObject requestBody = new JsonObject();
        requestBody.addProperty("document", user.getDocument());
        requestBody.addProperty("documentType", user.getDocumentType());
        requestBody.addProperty("password", user.getPassword());
        requestBody.addProperty("force", true);
        return requestBody;
    }
}
